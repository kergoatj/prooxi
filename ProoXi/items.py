# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field

__author__ = 'Alexandre Cloquet'


class ProoxiItem(Item):
    # define the fields for your item here like:
    # name = Field()
    url = Field()
    name = Field()
    description = Field()
    keyword = Field()
    link = Field()
    status = Field()
    pourcentage = Field()
    h2 = Field()
    currentCategory = Field()
    ip = Field()
