__author__ = 'Alexandre Cloquet'

from scrapy import log

HTTP = "http://"


class CheckUrl():
    blackListUrl = ['google',
                    'deviantart',
                    'linkedin',
                    'yahoo',
                    'lemonde',
                    'tripadvisor',
                    'flickr',
                    'twitter',
                    'facebook',
                    'viadeo',
                    'blogspot',
                    'doubleclick',
                    'akismet',
                    'twitter',
                    'amazon',
                    'adobe',
                    'viadeo',
                    'youtube'
                    ]

    def CheckUrl(self, urlInProgress="", urlTmp=""):
        lenUrlTmp = len(urlTmp)
        lenUrlInProgress = len(urlInProgress)
        if lenUrlTmp != 0 and lenUrlInProgress != 0:
            if not urlTmp[0:lenUrlInProgress] == urlInProgress:
                if urlTmp[0:len(HTTP)] == HTTP:
                    if self.isBlackListed(urlTmp):
                        log.msg("Url is black listed = " + urlTmp, level=log.ERROR)
                        return False
                    else:
                        return True

    def getNewUrl(self, urlTmp):
        urltmp = urlTmp.replace(HTTP, '')
        pos2 = urltmp.find('/')
        return HTTP + urltmp[0:pos2 + 1]

    def isBlackListed(self, url):
        for urlBlackListed in self.blackListUrl:
            if urlBlackListed in url:
                return True
            else:
                return False
